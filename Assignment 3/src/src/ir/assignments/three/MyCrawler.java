package src.ir.assignments.three;

import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.regex.Pattern;

import org.apache.http.Header;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class MyCrawler extends WebCrawler {

	// Data structures and variables
	private static String LOGPATH = null;
	private static FileHandler fileHandler;
    
	// Filters
//    private static final Pattern IMAGE_EXTENSIONS = Pattern.compile(".*(\\.(|pdf|rm|ps|eps|tex|doc|docx|xls|xlsx|names|data|dat|exe|bz2|tar|msi|bin|7z|psd|dmg|iso|epub|dll|cnf|tgz|sha1|thmx|mso|arff|rtf|jar|csv|smil|wmv|swf|wma|zip|rar|gz|ico|pfm|c|h|o))$");
	private static final Pattern EXTENSIONS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g|ico|"
					+ "|ppt|ppxt|iso|exe|msi|sha1|ogg|ogv|tgz"
					+ "|png|tiff?|mid|mp2|mp3|mp4|m4v|mkv"
					+ "|wav|avi|mov|mpeg|ram|m4v|pdf|uai"
					+ "|exe|bz2|tar|msi|bin|7z|dmg|iso|tgz|"
					+ "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
	private static final Pattern QUERY_SYMBOLS = Pattern.compile(".*[\\?@=].*");
	private static final Pattern BLACK_LIST = Pattern.compile("(http://)(calendar|archive|fano)(.ics.uci.edu/).*");
	
	public static void setLogPath (String logpath) {
		LOGPATH = logpath;
	}
	
	public void storeHTML (int docid, String html) {
		PrintWriter writer;
		String fileName = "./data/content/" + String.valueOf(docid) + ".txt";
		 try {
			 File htmlFile = new File(fileName);
			 if (!htmlFile.exists() && !htmlFile.isDirectory()) {
				 writer = new PrintWriter(fileName, "UTF-8");
				 writer.println(html);
				 writer.close();
			 }
		 } catch (FileNotFoundException e) {
			 e.printStackTrace();
		 } catch (UnsupportedEncodingException e) {
			 e.printStackTrace();
		 }
	}
	
	public void logInformation (String subDomain, String url, int docid) {
		PrintWriter writer;
		try {
			File log = new File(LOGPATH);
			if (log.exists() && !log.isDirectory()) {
				writer = new PrintWriter(new FileWriter(LOGPATH, true));
			}
			else {
				writer = new PrintWriter(LOGPATH, "UTF-8");
			}
			writer.println(subDomain + ", " + url + ", " + docid);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	* This method receives two parameters. The first parameter is the page
	 * in which we have discovered this new url and the second parameter is
	 * the new url. You should implement this function to specify whether
	 * the given url should be crawled or not (based on your crawling logic).
	 * In this example, we are instructing the crawler to ignore urls that
	 * have css, js, git, ... extensions and to only accept urls that start
	 * with "http://www.ics.uci.edu/". In this case, we didn't need the
	 * referringPage parameter to make the decision.
	 */
	@Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
		 String href = url.getURL().toLowerCase();
	 // Ignore the url if it has an extension that matches our defined set of image extensions.

	 if (EXTENSIONS.matcher(href).matches()) {
	   return false;
	 }
	 if (BLACK_LIST.matcher(href).matches()) {
		 return false;
	 }
	 if (QUERY_SYMBOLS.matcher(href).matches()) {
		 return false;
	 }
	 
	 // Only accept the url if it is in the "www.ics.uci.edu" domain and protocol is "http".
	 	return href.matches("(http://).*(.ics.uci.edu/).*");
	 }

    /**
      * This function is called when a page is fetched and ready
      * to be processed by your program.
      */
	@Override
	public void visit(Page page) {
	     int docid = page.getWebURL().getDocid();
	     String url = page.getWebURL().getURL();
	     String domain = page.getWebURL().getDomain();
	     String path = page.getWebURL().getPath();
	     String subDomain = page.getWebURL().getSubDomain();
	     String parentUrl = page.getWebURL().getParentUrl();
	     String anchor = page.getWebURL().getAnchor();
	     
	     logger.info("URL: {}", url);
	     if (LOGPATH != null) {
	    	 logInformation(subDomain, url, docid);
	     }

	     if (page.getParseData() instanceof HtmlParseData) {
	         HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
	         String text = htmlParseData.getText();
	         String html = htmlParseData.getHtml();
	         Set<WebURL> links = htmlParseData.getOutgoingUrls();
	         
	         if (html.length() != 0) {
	        	 storeHTML(docid, html);
	         }
	         System.out.println("Text length: " + text.length());
	         System.out.println("Html length: " + html.length());
	         System.out.println("Number of outgoing links: " + links.size());
	     }
	     
	     Header[] responseHeaders = page.getFetchResponseHeaders();
	     if (responseHeaders != null) {
	    	 logger.debug("Response headers:");
	    	 for (Header header : responseHeaders) {
	    		 logger.debug("\t{}: {}", header.getName(), header.getValue());
	    	 }
	     }

	     logger.debug("=============");
	}
}
